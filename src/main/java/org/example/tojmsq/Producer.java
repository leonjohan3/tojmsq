package org.example.tojmsq;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

public class Producer {
    private static final int MIN_ARG_COUNT = 2;

    private String providerUrl;
    private String queueName;
    private String filename;

    private Producer() {
    }

    public static void main(final String[] args) {
        new Producer().run(args);
    }

    private void run(final String[] args) {

        try {

            if (args.length < MIN_ARG_COUNT) {
                usage();
            } else {

                parseArgs(args);

                final Properties env = new Properties();
                env.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
                env.put(Context.PROVIDER_URL, providerUrl);
                final Context context = new InitialContext(env);
                final ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup("ConnectionFactory");

                Connection connection = null;
                Session session = null;

                try {

                    connection = connectionFactory.createConnection();
                    session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                    final Destination queue = session.createQueue(queueName);
                    final MessageProducer producer = session.createProducer(queue);
                    StringBuilder stringBuilder = new StringBuilder();

                    if (null != filename) {

                        stringBuilder = new StringBuilder(new String(Files.readAllBytes(Paths.get(filename))));

                    } else {

                        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {

                            int character;

                            while ((character = bufferedReader.read()) > -1) {
                                stringBuilder.append((char) character);
                            }
                        }
                    }

                    if (stringBuilder.length() > 0) {
                        final TextMessage textMessage = session.createTextMessage(stringBuilder.toString());
                        textMessage.setIntProperty("size", stringBuilder.length());

                        if (null != filename) {
                            textMessage.setJMSCorrelationID(filename);
                        } else {
                            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                            textMessage.setJMSCorrelationID("stdin-" + simpleDateFormat.format(new Date()));
                        }
                        producer.send(textMessage);
                    }

                } finally {
                    if (null != session) {
                        session.close();
                        session = null;
                    }
                    if (null != connection) {
                        connection.close();
                        connection = null;
                    }
                }
            }

        } catch (final Exception e) {
            e.printStackTrace();
            usage();
        }
    }

    private void usage() {
        System.err.println("\nUsage: java -jar tojmsq-1.0.0-SNAPSHOT.jar providerUrl queueName [filename]\n");
        System.err.println("    providerUrl: AciveMQ provider URL, e.g. tcp://localhost:61616");
        System.err.println("    queueName  : the queue name that will receive the JMS message");
        System.err.println("    filename   : optional. if not provided stdin will be used as input\n");
        System.exit(1);
    }

    private void parseArgs(final String[] args) {

        for (int i = 0; i < Math.min(MIN_ARG_COUNT + 1, args.length); i++) {

            if (0 == i) {
                providerUrl = args[i];
            } else {
                if (1 == i) {
                    queueName = args[i];
                } else {
                    filename = args[i];
                }
            }
        }
    }
}
